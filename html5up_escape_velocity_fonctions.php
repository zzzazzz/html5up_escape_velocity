<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {return;}

// Déclaration des blocs Z
$GLOBALS['z_blocs'] = array('content', 'head', 'head_js', 'header', 'footer');

// Activer HTML5 depuis le squelette
$GLOBALS['meta']['version_html_max'] = 'html5';

// Intertitres commençant par h2
$GLOBALS['debut_intertitre'] = "\n<h2 class=\"spip\">\n";
$GLOBALS['fin_intertitre'] = "</h2>\n";

// traitements tinytypo
if (!isset($GLOBALS['spip_pipeline']['affichage_final'])) {
	$GLOBALS['spip_pipeline']['affichage_final'] = '';
}
$GLOBALS['spip_pipeline']['affichage_final'] .= '|tinytypo';

function tinytypo($texte) {
	$texte = str_replace('hr class="spip"', 'hr', $texte);
	$texte = str_replace('spip_out', 'external', $texte);
	$texte = str_replace('spip_url', 'external', $texte);
	$texte = str_replace('spip_mail', 'mailto', $texte);
	$texte = str_replace('blockquote class="spip"', 'blockquote', $texte);
	$texte = str_replace('<table class="spip', '<table class="table spip', $texte);
	return $texte;
}